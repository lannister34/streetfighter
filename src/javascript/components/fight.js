import { controls } from '../../constants/controls';
import { KeyHandler } from '../helpers/keyHandler';
import { createElement} from "../helpers/domHelper";

export async function fight(firstFighter, secondFighter) {
  const arena = document.querySelector('.arena___root');

  const kickSound = createElement({
    'tagName': 'audio',
    'className': 'audio___kick',
    'attributes': {
      'src' : '../../../resources/kick.mp3'
    }
  });

  const blockedKickSound = createElement({
    'tagName': 'audio',
    'className': 'audio___kick-blocked',
    'attributes': {
      'src' : '../../../resources/kick-blocked.mp3'
    }
  });

  const criticalKickSound = createElement({
    'tagName': 'audio',
    'className': 'audio___kick-critical',
    'attributes': {
      'src' : '../../../resources/kick-critical.mp3'
    }
  });

  arena.append(kickSound, blockedKickSound, criticalKickSound);

  firstFighter.position = 'left';
  secondFighter.position = 'right';

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    const makeDamage = (function() {

      const firstHealthBar = document.getElementById('left-fighter-indicator');
      const secondHealthBar = document.getElementById('right-fighter-indicator');

      let firstHealthPoints = firstFighter.health;
      let secondHealthPoints = secondFighter.health;

      let firstCritTimer, secondCritTimer;

      return function(attacker, defender, isCritical, isBlocked) {
        if (isCritical) {
          if (attacker.position === firstFighter.position) {
            if (firstCritTimer) {
              return;
            } else {
              criticalKickSound.play();
              firstCritTimer = window.setTimeout(() => {
                window.clearTimeout(firstCritTimer);
                firstCritTimer = null;
              }, 10000)
            }
          } else {
            if (secondCritTimer) {
              return;
            } else {
              criticalKickSound.play();
              secondCritTimer = window.setTimeout(() => {
                window.clearTimeout(secondCritTimer);
                secondCritTimer = null;
              }, 10000)
            }
          }
        };

        isBlocked ?
            blockedKickSound.play() :
            kickSound.play();

        const damage = getDamage(...arguments);
        if (!damage) {
          return;
        } else {
          if (defender.position === firstFighter.position) {
            firstHealthPoints -= damage;
            firstHealthPoints < 0 && (firstHealthPoints = 0);
            firstHealthBar.style.width = ((firstHealthPoints * 100) / firstFighter.health) + '%';
          } else {
            secondHealthPoints -= damage;
            secondHealthPoints < 0 && (secondHealthPoints = 0);
            secondHealthBar.style.width = ((secondHealthPoints * 100) / secondFighter.health) + '%';
          };

          if(firstHealthPoints === 0 || secondHealthPoints === 0) {
            handler.removeListeners();
            resolve(firstHealthPoints <= 0 ? secondFighter : firstFighter);
          }
        };
      }
    })();

    const triggers = new Map();
    const keys = [{}, {}];

    for (let name in controls) {
      if (typeof controls[name] === 'string') {

        if (/attack/i.test(name)) {
          /one/i.test(name) ?
              keys[0]['attack'] = controls[name] :
              keys[1]['attack'] = controls[name];
        } else {
          /one/i.test(name) ?
              keys[0]['block'] = controls[name] :
              keys[1]['block'] = controls[name];
        }
      } else if (Array.isArray(controls[name])) {
        /one/i.test(name) ?
            keys[0]['combination'] = controls[name] :
            keys[1]['combination'] = controls[name];
      };
    };

    triggers.set(keys[0]['attack'], {
      'action': makeDamage.bind(this, firstFighter, secondFighter, false),
      'arguments': [keys[1]['block']],
      'nonConditions': [keys[0]['block']]
    });

    triggers.set(keys[0]['block'], {});

    keys[0]['combination'].forEach((key, index, array) => {
      triggers.set(key, {
        'action': makeDamage.bind(this, firstFighter, secondFighter, true),
        'conditions': array
      });
    });

    triggers.set(keys[1]['attack'], {
      'action': makeDamage.bind(this, secondFighter, firstFighter, false),
      'arguments': [keys[0]['block']],
      'nonConditions': [keys[1]['block']]
    });

    triggers.set(keys[1]['block'], {});

    keys[1]['combination'].forEach((key, index, array) => {
      triggers.set(key, {
        'action': makeDamage.bind(this, secondFighter, firstFighter, true),
        'conditions': array
      });
    });

    const handler = new KeyHandler(triggers);

  });
}

export function getDamage(attacker, defender, isCritical, isBlocked) {
  if (isBlocked) {
    return 0;
  };

  const damage = getHitPower(attacker, isCritical);
  const block = !isCritical ? getBlockPower(defender) : 0;

  return damage - block > 0 ? damage - block : 0;
}

export function getHitPower(fighter, isCritical) {
  // return hit power
  return fighter.attack * (isCritical ? 2 : Math.random() + 1);
}

export function getBlockPower(fighter) {
  // return block power
  return fighter.defense * (Math.random() + 1);
}
