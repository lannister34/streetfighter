import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  if (fighter) {
    const fighterImage = createElement({
      tagName: 'img',
      attributes: {
        src: fighter.source
      }
    });

    const fighterDetails = createElement({
      tagName: 'div',
      className: 'fighter-preview___info'
    });

    const fighterName = createElement({
      tagName: 'span',
      className: 'fighter-preview___name'
    });

    fighterName.innerHTML = fighter.name;

    //attack

    const attackBlock = createElement({
      tagName: 'div',
      className: 'fighter-preview___stat-block'
    });

    const attackBlank = createElement({
      tagName: 'div',
      className: 'fighter-preview___stat fighter-preview___attack-blank'
    });

    const attackValue = createElement({
      tagName: 'div',
      className: 'fighter-preview___stat fighter-preview___stat-value fighter-preview___attack-value',
      attributes: {
        style: "width: " + fighter.attack * 197 / 5 + 'px'
      }
    });

    const attackTextValue = createElement({
     tagName: 'span',
     className: 'fighter-preview___text-value'
    });

    attackTextValue.innerHTML = fighter.attack;

    //health

    const healthBlock = createElement({
      tagName: 'div',
      className: 'fighter-preview___stat-block'
    });

    const healthBlank = createElement({
      tagName: 'div',
      className: 'fighter-preview___stat fighter-preview___health-blank'
    });

    const healthValue = createElement({
      tagName: 'div',
      className: 'fighter-preview___stat fighter-preview___stat-value fighter-preview___health-value',
      attributes: {
        style: "width: " + fighter.health * 197 / 100 + 'px'
      }
    });

    const healthTextValue = createElement({
      tagName: 'span',
      className: 'fighter-preview___text-value'
    });

    healthTextValue.innerHTML = fighter.health;

    //defense

    const defenseBlock = createElement({
      tagName: 'div',
      className: 'fighter-preview___stat-block'
    });

    const defenseBlank = createElement({
      tagName: 'div',
      className: 'fighter-preview___stat fighter-preview___defense-blank'
    });

    const defenseValue = createElement({
      tagName: 'div',
      className: 'fighter-preview___stat fighter-preview___stat-value fighter-preview___defense-value',
      attributes: {
        style: "width: " + fighter.defense * 197 / 5 + 'px'
      }
    });

    const defenseTextValue = createElement({
      tagName: 'span',
      className: 'fighter-preview___text-value'
    });

    defenseTextValue.innerHTML = fighter.defense;

    //build node tree

    attackBlock.appendChild(attackBlank);
    attackBlock.appendChild(attackValue);
    attackBlock.appendChild(attackTextValue);

    healthBlock.appendChild(healthBlank);
    healthBlock.appendChild(healthValue);
    healthBlock.appendChild(healthTextValue);

    defenseBlock.appendChild(defenseBlank);
    defenseBlock.appendChild(defenseValue);
    defenseBlock.appendChild(defenseTextValue);

    fighterDetails.appendChild(fighterName);
    fighterDetails.appendChild(attackBlock);
    fighterDetails.appendChild(healthBlock);
    fighterDetails.appendChild(defenseBlock);

    fighterElement.appendChild(fighterImage);
    fighterElement.appendChild(fighterDetails);
  };

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
