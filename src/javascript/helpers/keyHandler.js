import { getDamage, getHitPower, getBlockPower } from '../components/fight';

export class KeyHandler {

    constructor(triggers) {
        this.keys = new Map();

        for (let item of triggers) {
          this.keys.set(item[0], false);
        };

        this.triggers = triggers;

        const handleDown = this.keydownHandler.bind(this);
        const handleUp = this.keyupHandler.bind(this);

        this.removeListeners = function() {
          window.removeEventListener('keydown', handleDown, false);
          window.removeEventListener('keyup', handleUp, false);
        };

        window.addEventListener('keydown', handleDown, false);
        window.addEventListener('keyup', handleUp, false);
    }

     keydownHandler(e) {
        if (this.keys.get(e.code) === false) {
            this.keys.set(e.code, true);

            const trigger = this.triggers.get(e.code);

            if (!trigger || !trigger.action) {
                return;
            }

            if (trigger.conditions) {
                if (trigger.conditions.some((key) => {
                    return !this.keys.get(key)
                })) {
                    return;
                };
            };

            if (trigger.nonConditions) {
                if (trigger.nonConditions.some((key) => {
                    return this.keys.get(key)
                })) {
                    return;
                };
            };

            let args = [];

            if (trigger.arguments) {
                args = trigger.arguments.map((key) => {
                    return this.keys.get(key);
                });
            };

            trigger.action(...args);
        }
    }

    keyupHandler(e) {
        this.keys.has(e.code) && (this.keys.set(e.code, false));
    }

}